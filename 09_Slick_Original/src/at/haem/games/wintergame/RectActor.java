package at.haem.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;


public class RectActor implements Actor{
	private int x;
	private int y;
	private direction dir;
	
	public enum direction{
		right,left,up,down
	}
	
	public RectActor(int x, int y, direction dir) {
		super();
		this.x = x;
		this.y = y;
		this.dir = dir;
		
	}
	
	public void render(Graphics graphics) {
		graphics.drawRect(this.x, this.y, 50, 50);
	}
	
	
	
	public void update(GameContainer gc, int delta) {
		if(dir == direction.right) {
			this.x++;
			if(this.x >= 700 && this.y <= 100) {
				this.dir = direction.down;
			}
		}
		else if(dir == direction.down) {
			this.y++;
			if(this.x >= 700 && this.y >=500) {
				this.dir = direction.left;
			}
		}
		else if(dir == direction.left) {
			this.x--;
			if(this.x <= 100 && this.y >=500) {
				this.dir = direction.up;
			}
		}
		else if(dir == direction.up) {
			this.y--;
			if(this.x <= 100 && this.y <=100) {
				this.dir = direction.right;
			}
		}
	}
	
	
}
