package at.haem.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.haem.games.wintergame.RectActor.direction;

public class OvalActor implements Actor{
	private int x;
	private int y;
	private direction dir;
	
	public enum direction{
		right,left
	}
	
	
	public OvalActor(int x, int y, direction dir) {
		super();
		this.x = x;
		this.y = y;
		this.dir = dir;
	}
	
	public void render(Graphics graphics) {
		graphics.drawOval(this.x, this.y, 80, 50);
	}
	
	
	
	public void update(GameContainer gc, int delta) {
		if(dir == direction.right) {
			this.x++;
			if(this.x >= 700) {
				this.dir = direction.left;
			}
		}
		else{
			this.x--;
			if(this.x <= 100) {
				this.dir = direction.right;
			}
		}
	}
}
