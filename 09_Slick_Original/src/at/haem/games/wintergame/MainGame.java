package at.haem.games.wintergame;


import java.util.ArrayList;
import java.util.List;

import java.util.Random;

import org.lwjgl.opengl.Drawable;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

import at.haem.games.wintergame.RectActor.direction;

public class MainGame extends BasicGame{
	
	private List<Actor> actors;
	private ShootingStar shootingStar;
	
	
	
	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics ) throws SlickException {
		//zeichnen
		for (Actor actor : this.actors) {
			actor.render(graphics);
			
		}
		
		
		
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		//wird nur 1 mal aufgerufen am anfang
		this.actors = new ArrayList<>();
		
		this.actors.add(new RectActor(100, 100,RectActor.direction.right));
		this.actors.add(new OvalActor(100, 400,OvalActor.direction.right));
		this.actors.add(new CircleActor(350, 100));
		this.actors.add(new Snowman(gc));
		this.shootingStar = new ShootingStar();
		this.actors.add(this.shootingStar);
		
		
		
		
		for(int i = 0; i<20; i++) {
			this.actors.add(new Snowflake(Snowflake.Size.small));
		}
		for(int i = 0; i<20; i++) {
			this.actors.add(new Snowflake(Snowflake.Size.medium));
		}
		for(int i = 0; i<20; i++) {
			this.actors.add(new Snowflake(Snowflake.Size.big));
		}
		
		
		
		
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		//delta = zeit seit dem letzten aufrufen
		
		for (Actor actor : this.actors) {
			actor.update(gc,delta);
		}
		
		
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
