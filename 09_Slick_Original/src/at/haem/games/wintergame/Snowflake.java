package at.haem.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import java.util.Random;

public class Snowflake implements Actor{
	
	private float width;
	private float height;
	private float x;
	private float y;
	private float speed;
	Random randX;
	Random randY;
	
	private Size siz;
	public enum Size{
		small,medium,big
	}
	
	public Snowflake(Size siz) {
		super();
		this.randX = new Random();
		this.x = randX.nextInt(799) + 1;
		this.randY = new Random();
		this.y = randY.nextInt(600) + 0;
		this.y *= -1;
		this.siz = siz;
		
		if(this.siz == Size.small) {
			this.width = 10;
			this.height = 10;
			this.speed = 0.3f;
		}
		if(this.siz == Size.medium) {
			this.width = 15;
			this.height = 15;
			this.speed = 0.4f;
		}
		if(this.siz == Size.big) {
			this.width = 20;
			this.height = 20;
			this.speed = 0.5f;
		}
	}
	
	public void render(Graphics graphics) {
		
		
		graphics.fillOval(this.x, this.y, this.width, this.height);
	}
	
	
	
	public void update(GameContainer gc, int delta) {
		if(this.y <= 600) {
			this.y += delta * this.speed;
		}
		else{
			this.y = randY.nextInt(600) + 0;
			this.y *= -1;
			this.x = randX.nextInt(799) + 1;
		}
	}
	
	
	
}
