package at.haem.games.wintergame;



import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Snowman implements Actor{

	private float width1;
	private float height1;
	private float width2;
	private float height2;
	private float width3;
	private float height3;
	private float x;
	private float y;
	private boolean right;
	private static Input in;
	
	private Image snowman;
	
	
	
	public Snowman(GameContainer gc) throws SlickException {
		super();
		this.x = 50;
		this.y = 50;
		this.right = true;
		this.snowman = new Image("testdata/snowman.png");
		this.in = gc.getInput();
		
	}

	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		snowman.draw(this.x,this.y);
		
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		
		if(this.in.isKeyDown(in.KEY_UP)) {
			this.y--;
		}
		else if(this.in.isKeyDown(in.KEY_DOWN)) {
			this.y++;
		}
	}

}
