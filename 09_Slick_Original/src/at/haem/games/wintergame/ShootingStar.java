package at.haem.games.wintergame;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class ShootingStar implements Actor{
	private float width;
	private float height;
	private float x;
	private float y;
	private int counter;
	private int yellow;
	private int blue;
	private boolean toRight = false;
	private boolean toLeft = false;
	private boolean up;
	private float speedX;
	private float speedY;
	private int timer;
	Random random;
	
	
	
	
	public ShootingStar() {
		super();
		resetShootingStar();
	}



	private void resetShootingStar() {
		this.width = 38;
		this.height = 38;
		
		this.random = new Random();
		
		
		
		this.toRight = false;
		this.toLeft = false;
		this.up = true;
		
		setRandomPosition();
		setSpeed();
		
		this.counter = 350;
		this.yellow = 247;
		this.timer = random.nextInt(4500) + 0;
		this.blue = 0;
	}
	
	private void setSpeed() {
		this.speedY = 0.001f;
		this.speedX = 0.9f;
		
	}


	public void setRandomPosition() {
		this.x = random.nextInt(600) + 50;
		this.y = random.nextInt(150) + 50;
		if (this.x <= 400) {
			this.toRight = true;
		}
		else{
			this.toLeft = true;
		}
	}
	
	
	@Override
	public void render(Graphics graphics) {
		// TODO Auto-generated method stub
		
		if (this.height > 0 && this.width > 0) {
			graphics.setColor(new Color(255,this.yellow,0 + this.blue));
			//graphics.fillOval(this.x, this.y, this.width, this.height);
			graphics.fillOval(this.x, this.y, this.width, this.height, 4);
			graphics.setColor(Color.white);
			
		 
		 }
	}
	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		
		if(this.toRight == true) {
			this.x += delta * this.speedX;
		}
		else if(this.toLeft == true) {
			this.x -= delta * this.speedX;
		}
		
		if(this.counter >= 0 && this.up == true) {
			this.y -= delta * this.counter * this.speedY;
			this.counter--;
		}
		else if(this.counter <= 350 && this.up == false) {
			this.y += delta * this.counter * this.speedY;
			this.counter++;
		}
		else if(this.counter <= 0 && this.up == true) {
			this.up = false;
		}
		
		if (this.height > 0 && this.width > 0) {
			this.height -= 0.05;
			this.width -= 0.05;
		}
		if (this.yellow < 255) {
			this.yellow ++;
		}
		this.blue++;
		
		if(this.timer >= 6000) {
			resetShootingStar();	
		}
		this.timer++;
			
	}
}
