package at.haem.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CircleActor implements Actor{
	private int x;
	private int y;
	
	
	public CircleActor(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public void render(Graphics graphics) {
		graphics.drawOval(this.x, this.y, 50, 50);
	}
	
	
	
	public void update(GameContainer gc, int delta) {
		if(this.y <= 600) {
			this.y++;
		}
		else{
			this.y = 0;
		}
	}
}
